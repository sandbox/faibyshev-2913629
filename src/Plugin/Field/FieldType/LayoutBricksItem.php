<?php

namespace Drupal\bricks_layouts\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem;

/**
 * {@inheritdoc}
 *
 * @FieldType(
 *   id = "layout_bricks",
 *   label = @Translation("Layout Bricks"),
 *   description = @Translation("An entity field containing a tree of entity reference layouts and bricks."),
 *   category = @Translation("Reference"),
 *   default_widget = "bricks_layout_entity_browser_entity_reference",
 *   default_formatter = "layout_bricks_nested",
 *   list_class = "\Drupal\Core\Field\EntityReferenceFieldItemList",
 * )
 */
class LayoutBricksItem extends EntityReferenceItem {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = parent::propertyDefinitions($field_definition);

    _bricks_layouts_field_properties_alter($properties);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    $schema = parent::schema($field_definition);

    _bricks_layouts_field_schema_alter($schema);

    return $schema;
  }

}
