<?php

namespace Drupal\bricks_layouts\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter;

/**
 * {@inheritdoc}
 *
 * @FieldFormatter(
 *   id = "layout_bricks_nested",
 *   label = @Translation("Layout Bricks (Nested)"),
 *   description = @Translation("Display the referenced entities recursively rendered by entity_view()."),
 *   field_types = {
 *     "layout_bricks",
 *     "layout_bricks_revisioned"
 *   }
 * )
 */
class LayoutBricksNestedFormatter extends EntityReferenceEntityFormatter {
}
