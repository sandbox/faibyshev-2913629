<?php

namespace Drupal\bricks_layouts\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity_browser\Plugin\Field\FieldWidget\EntityReferenceBrowserWidget;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Component\Utility\NestedArray;

/**
 * Plugin implementation of the 'entity_reference' widget for entity browser.
 *
 * @FieldWidget(
 *   id = "bricks_layout_entity_browser_entity_reference",
 *   label = @Translation("Bricks layout entity browser"),
 *   description = @Translation("Uses entity browser to select entities."),
 *   multiple_values = TRUE,
 *   field_types = {
 *     "layout_bricks",
 *     "layout_bricks_revisioned"
 *   }
 * )
 */
class BricksLayoutEntityReferenceBrowserWidget extends EntityReferenceBrowserWidget {

  /**
   * The depth of the delete button.
   *
   * This property exists so it can be changed if subclasses.
   *
   * @var int
   */
  protected static $deleteDepth = 4;

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = parent::formElement($items, $delta, $element, $form, $form_state);
    $element['target_id']['#ajax']['callback'][1] = 'updateBricksWidgetCallback';
    /** @var \Drupal\Core\Template\TwigEnvironment $twig */
    $twig = \Drupal::getContainer()->get('twig');
    $layoutService = \Drupal::service('plugin.manager.core.layout');
    $fieldName = $items->getName();
    // Load current layout.
    $currentLayout = '';
    $clear = TRUE;
    foreach ($items->getValue() as $item) {
      // TODO: refactor.
      if (!empty($item['layout'])) {
        $entityLayout = $item['layout'];
      }
      if (!empty($entityLayout)) {
        if ($clear) {
          $clear = FALSE;
          $currentLayout = $entityLayout;
        }
        elseif ($currentLayout != $entityLayout) {
          $currentLayout = '';
        }
      }

    }
    if ($userInput = $form_state->getUserInput()) {
      $inputLayout = $userInput[$fieldName]['widget_layout'];
    }
    $layout_options = [];
    $default_options = $this->getSetting('layouts');
    foreach ($layoutService->getGroupedDefinitions() as $category => $layout_definitions) {
      foreach ($layout_definitions as $name => $layout_definition) {
        if (!in_array($name, $default_options)) {
          continue;
        }
        $layout_options[$category][$name] = $layout_definition->getLabel();
      }
    }
    $currentLayout = (!empty($inputLayout)) ? $inputLayout : $currentLayout;
    $element['widget_layout'] = [
      '#title' => t('Layout'),
      '#type' => 'select',
      '#empty_option' => $this->t('- Select -'),
      '#options' => $layout_options,
      '#default_value' => (!empty($currentLayout) ? $currentLayout : ''),
      '#ajax' => [
        'callback' => [get_class($this), 'ajaxDefaultRegionCallback'],
        'event' => 'change',
      ],
      '#weight' => '-100',
      '#layout_select_item' => TRUE,
      '#required' => TRUE,
      '#attributes' => [
        'class' => ['widget-layout-select'],
      ],
      '#element_validate' => [
        [$this, 'validateLayout'],
      ],
    ];

    $newCount = (int) (count($element['current']['items']) - count($items));
    $triggered = $form_state->getTriggeringElement();
    $triggering = ($triggered['#parents']) ? end($triggered['#parents']) : NULL;

    if ($newCount < 0 && $triggering == 'remove_button') {
      // Check state if we already remove item.
      $field = $form_state->getValue($fieldName);
      if ($field['#removed'] === TRUE) {
        // Remove.
        $deletedItems = [$field['#removed_id']];
        $element['target_id']['#deleted_items'] = $deletedItems;
        $newItems = $this->unsetItems($items->getValue(), $deletedItems);
        $itemsObject = [];
        foreach ($newItems as $newItem) {
          $itemsObject[] = (object) $newItem;
        }
        $items = $itemsObject;
      }
    }
    // Hack end.
    foreach ($element['current']['items'] as $delta => $currentItem) {
      $item = $items[$delta];
      $itemLayout = !empty($item->layout) ? $item->layout : $currentLayout;
      $region = !empty($item->region) ? $item->region : '';
      $depth = !empty($item->depth) ? $item->depth : 0;
      $element['current']['items'][$delta]['layout'] = [
        '#type' => 'hidden',
        '#default_value' => $itemLayout,
        '#weight' => 10,
        '#attributes' => [
          'class' => ['bricks-layout-layout'],
        ],
      ];
      $element['current']['items'][$delta]['region'] = [
        '#type' => 'hidden',
        '#default_value' => $region,
        '#weight' => 10,
        '#attributes' => [
          'class' => ['bricks-layout-region'],
        ],
      ];
      $element['current']['items'][$delta]['depth'] = [
        '#type' => 'hidden',
        '#default_value' => $depth,
        '#weight' => 10,
        '#attributes' => [
          'class' => ['bricks-layout-depth'],
        ],
      ];
      $element['current']['items'][$delta]['#attributes'] = array_merge($element['current']['items'][$delta]['#attributes'], [
        'draggable' => "true",
        'ondragstart' => "drag(event)",
        'ondragend' => "dragEnd(event)",
        'id' => $fieldName . "-drag-" . $delta,
      ]);
      // Unset label class (ugly style).
      unset($element['current']['items'][$delta]['#attributes']['class'][1]);
      $element['current']['items'][$delta]['#attributes']['class'][] = 'draggable-element';
      $element['current']['items'][$delta]['remove_button']['#ajax']['callback'][1] = 'updateBricksWidgetCallback';
      // Add extra CSS classes.
      $element['current']['items'][$delta]['remove_button']['#attributes']['class'][] = 'action-item';
      $element['current']['items'][$delta]['edit_button']['#attributes']['class'][] = 'action-item';
      $element['current']['items'][$delta]['display']['#attributes']['class'][] = 'label-item';
      // Add entity type.
      if (isset($element['entity_browser']['#default_value'][$delta])) {
        $element['current']['items'][$delta]['entity_type'] = [
          '#markup' => '[' . $element['entity_browser']['#default_value'][$delta]->getType() . ']',
        ];
      }

    }
    $element['#attached']['library'][] = 'bricks_layouts/bricks_layouts';
    $html = '<div class="panels-dnd need-rebuild">';
    if (!empty($currentLayout) && self::checkAvailabilityLayout($currentLayout)) {
      $layout = $layoutService->getDefinition($currentLayout);
      if ($layout) {
        $html .= $this->renderLayoutForm($layout, $twig);
      }
    }
    $html .= '</div>';
    // Load null region.
    $path = drupal_get_path('module', 'bricks_layouts');
    $tempPath = $path . '/templates/default-null-region.html.twig';
    if (file_exists($tempPath)) {
      $template = $twig->loadTemplate($tempPath);
      $html .= $template->render([]);
    }

    $element['layout_wrapper'] = [
      '#type' => 'inline_template',
      '#template' => $html,
    ];
    $element['#attributes']['class'][] = 'widgets-list-wrapper';

    return $element;
  }

  /**
   * Implements callback for Ajax event on layout change.
   *
   * @param array $form
   *   From render array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current state of form.
   *
   * @return object
   *   Ajax AjaxResponse
   */
  public static function ajaxDefaultRegionCallback(array &$form, FormStateInterface $form_state) {
    $element = $form_state->getTriggeringElement();
    $ajax_response = new AjaxResponse();
    $ajax_response->addCommand(new InvokeCommand(NULL, 'allToNullRegion', []));
    /** @var \Drupal\Core\Layout\LayoutPluginManager $layouts */
    $layouts = \Drupal::service('plugin.manager.core.layout')->getDefinitions();
    $layout = $element['#value'];
    if ($layout != '0' && self::checkAvailabilityLayout($layout)) {
      $currentLayout = $layouts[$layout];
      /** @var \Drupal\Core\Template\TwigEnvironment $twig */
      $twig = \Drupal::getContainer()->get('twig');
      $ajax_response->addCommand(new HtmlCommand(
        '.panels-dnd', self::renderLayoutForm($currentLayout, $twig)
      ));
    }
    return $ajax_response;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    $entities = empty($values['target_id']) ? [] : explode(' ', trim($values['target_id']));
    $return = [];
    foreach ($entities as $delta => $entity) {
      $return[] = [
        'target_id' => explode(':', $entity)[1],
        'layout' => $values['current']['items'][$delta]['layout'],
        'depth' => $values['current']['items'][$delta]['depth'],
        'region' => $values['current']['items'][$delta]['region'],
      ];
    }
    return $return;
  }

  /**
   * Render layout form.
   */
  public function renderLayoutForm($currentLayout, $twig) {
    $provider = $currentLayout->get('provider');
    if ($provider != 'layout_discovery') {
      $path = $currentLayout->get('path');
      $additional = $currentLayout->get('additional');
      $tempPath = $path . '/' . $additional['form_template'] . '.html.twig';
    }
    else {
      $path = drupal_get_path('module', 'bricks_layouts');
      $tempPath = $path . '/templates/core/layout-form-' . $currentLayout->get('id') . '.html.twig';
    }
    if (file_exists($tempPath)) {
      $template = $twig->loadTemplate($tempPath);
      return $template->render([]);
    }
    return FALSE;
  }

  /**
   * AJAX form callback.
   */
  public static function updateBricksWidgetCallback(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    // AJAX requests can be triggered by hidden "target_id" element when
    // entities are added or by one of the "Remove" buttons. Depending on that
    // we need to figure out where root of the widget is in the form structure
    // and use this information to return correct part of the form.
    $mainFiledName = preg_replace('/\[.*\]/', '', $trigger['#name']);
    if (!empty($trigger['#ajax']['event']) && $trigger['#ajax']['event'] == 'entity_browser_value_updated') {
      $value = $form[$mainFiledName]['widget']['target_id']['#value'];
      $new = '';
      for ($i = 0; $i < $trigger['#new_count']; $i++) {
        $new = substr($value, 0, strpos(trim($value), ' ')) . ' ' . $new;
        $value = preg_replace('~^(.*?)\s~', '', $value);
      }
      if (!empty($new)) {
        $value = $value . ' ' . trim($new);
      }
      $parents = array_slice($trigger['#array_parents'], 0, -1);
      $form[$mainFiledName]['widget']['target_id']['#value'] = $value;
      $form[$mainFiledName]['widget']['target_id']['#default_value'] = $value;
    }
    elseif ($trigger['#type'] == 'submit' && strpos($trigger['#name'], '_remove_')) {
      $parents = array_slice($trigger['#array_parents'], 0, -static::$deleteDepth);
    }

    return NestedArray::getValue($form, $parents);
  }

  /**
   * Unset items.
   *
   * @param array $source
   *   Source array.
   * @param array $toDelete
   *   Array with keys.
   *
   * @return array
   *   Result.
   */
  public function unsetItems(array $source, array $toDelete) {
    $result = [];
    ksort($source);
    foreach ($source as $key => $value) {
      if (!in_array($key, $toDelete)) {
        $result[] = $value;
      }
    }
    return $result;
  }

  /**
   * Submit callback for remove buttons.
   */
  public static function removeItemSubmit(&$form, FormStateInterface $form_state) {
    $triggering_element = $form_state->getTriggeringElement();
    if (!empty($triggering_element['#attributes']['data-entity-id']) && isset($triggering_element['#attributes']['data-row-id'])) {
      $id = $triggering_element['#attributes']['data-entity-id'];
      $row_id = $triggering_element['#attributes']['data-row-id'];
      $parents = array_slice($triggering_element['#parents'], 0, -static::$deleteDepth);
      $array_parents = array_slice($triggering_element['#array_parents'], 0, -static::$deleteDepth);
      $fieldName = $triggering_element['#parents'][0];
      // Find and remove correct entity.
      $values = explode(' ', $form_state->getValue(array_merge($parents, ['target_id'])));
      foreach ($values as $index => $item) {
        if ($item == $id && $index == $row_id) {
          array_splice($values, $index, 1);

          break;
        }
      }
      $target_id_value = implode(' ', $values);

      // Set new value for this widget.
      $target_id_element = &NestedArray::getValue($form, array_merge($array_parents, ['target_id']));
      $form_state->setValueForElement($target_id_element, $target_id_value);
      NestedArray::setValue($form_state->getUserInput(), $target_id_element['#parents'], $target_id_value);
      $state = $form_state->getValue($fieldName);
      $state['current']['items'] = self::unsetItems($state['current']['items'], [$row_id]);
      $state['#removed'] = TRUE;
      $state['#removed_id'] = $row_id;
      $form_state->setValue($fieldName, $state);
      $userInput = $form_state->getUserInput();
      $userInput[$fieldName]['current']['items'] = self::unsetItems($userInput[$fieldName]['current']['items'], [$row_id]);
      $form_state->setUserInput($userInput);

      // Rebuild form.
      $form_state->setRebuild();
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'layouts' => [],
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);
    $layoutService = \Drupal::service('plugin.manager.core.layout');
    $element['layouts'] = [
      '#type' => 'select',
      '#title' => t('Available layouts'),
      '#multiple' => TRUE,
      '#options' => $layoutService->getLayoutOptions(),
      '#empty_option' => $this->t('-select-'),
      '#default_value' => $this->getSetting('layouts'),
    ];

    return $element;
  }

  /**
   * Check availability Layout.
   *
   * @param string $value
   *   Layout string.
   *
   * @return bool
   *   Layout exist or not.
   */
  public static function checkAvailabilityLayout($value) {
    $layouts = \Drupal::service('plugin.manager.core.layout')->getDefinitions();
    if (!empty($layouts[$value])) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Validate the layout field.
   */
  public function validateLayout($element, FormStateInterface $form_state) {
    $value = $element['#value'];
    $default_options = $this->getSetting('layouts');
    if (!in_array($value, $default_options)) {
      $form_state->setError($element, t("Layout isn't available."));
    }
  }

}
