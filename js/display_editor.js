/**
 * @file
 * Bricks Layouts.
 */

function allowDrop(ev) {
  ev.preventDefault();
}

function drag(ev) {
  ev.dataTransfer.setData("text", ev.target.id);
}

function drop(element, ev) {
  ev.preventDefault();
  var data = ev.dataTransfer.getData("text");
  ev.target.appendChild(document.getElementById(data));
  setNewRegion(event.target.id, element.getAttribute('data-region'));
  recalculateDepth(element);

}

function dragEnd(event) {
  var id = event.target.id;
}

setNewRegion = null;
rebuildLayoutFull = null;
recalculateDepth = null;

(function ($) {
  function JQsetNewRegion(item_id, region_id) {
    var currentLayout = $('.widget-layout-select').find(":selected").val();
    var selector = '#' + item_id;
    $(selector).find('.bricks-layout-region').val(region_id);
    $(selector).find('.bricks-layout-layout').val(currentLayout);
  }

  function JQrecalculateDepth(element) {
    var depth = 0;
    $(element).find('.draggable-element').each(function () {
      var self = this;
      $(self).find('.bricks-layout-depth').each(function (index, el) {
        $(el).val(depth);
        depth = depth + 1;
      });
    });
  }

  function JQrebuildLayoutFull() {
    $(document).find('.draggable-element').each(function () {
      var self = this;
      $(document).find('#region-null').each(function () {
        var null_region = this;
        null_region.appendChild(self);
      });
    });

    var regions = [];
    $(document).find('.region-zone').each(function (index, el) {
      var self = this;
      var regionContent = [];
      regions[el.id] = regionContent;
    });

    $(document).find('.draggable-element').each(function (index, el) {
      var self = this;
      var region = 'null';
      var depth = 0;
      $(self).find('.bricks-layout-depth').each(function (index, el) {
        depth = $(el).val();
      });
      $(self).find('.bricks-layout-region').each(function (index, el) {
        if ($(el).val() !== "") {
          region = $(el).val();
        }
      });
      var check = false;
      while (!check) {
        if (depth in regions['region-' + region]) {
          depth = +depth + 1;
        }
        else {
          regions['region-' + region][depth] = self;
          check = true;
        }
      }

    });
    for (var key in regions) {
      var regionId = key;
      for (var depthKey in regions[regionId]) {
        $(document).find('#' + regionId).each(function (index, el) {
          var new_region = this;
          new_region.appendChild(regions[regionId][depthKey]);
        });
      }
    }
  }

  setNewRegion = JQsetNewRegion;
  rebuildLayoutFull = JQrebuildLayoutFull;
  recalculateDepth = JQrecalculateDepth;

  $.fn.allToNullRegion = function () {
    $(document).find('.draggable-element').each(function () {
      var self = this;
      $(document).find('#region-null').each(function () {
        var null_region = this;
        null_region.appendChild(self);
        setNewRegion(this.id, '');
      });
    });
  };
})(jQuery);

(function ($, Drupal, drupalSettings, document) {
  "use strict";
  /**
   * @type {{attach: attach}}
   */
  Drupal.behaviors.bricksLayout = {
    attach: function (context) {
      $(context).find('.panels-dnd').each(function () {
        var self = this;
        if ($(self).hasClass('need-rebuild')) {
          rebuildLayoutFull();
          $(self).removeClass('need-rebuild');
        }
      });
    }
  };

}(jQuery, Drupal, drupalSettings, document));
