<?php

/**
 * @file
 * Allows to work Bricks and Widget Engine together.
 */

use Drupal\Core\TypedData\DataDefinition;

/**
 * Helper function for FieldItemInterface::propertyDefinitions().
 */
function _bricks_layouts_field_properties_alter(&$properties) {
  $properties['depth'] = DataDefinition::create('integer')
    ->setLabel(t('Depth'));
  $properties['layout'] = DataDefinition::create('string')
    ->setLabel(t('Layout'));
  $properties['region'] = DataDefinition::create('string')
    ->setLabel(t('Region'));
  $properties['options'] = DataDefinition::create('any')
    ->setLabel(t('Options'));

}

/**
 * Helper function for FieldItemInterface::schema().
 */
function _bricks_layouts_field_schema_alter(&$schema) {
  $schema['columns']['depth'] = [
    'type' => 'int',
    'size' => 'tiny',
    'unsigned' => TRUE,
  ];

  $schema['columns']['options'] = [
    'type' => 'blob',
    'size' => 'normal',
    'not null' => FALSE,
    'serialize' => TRUE,
  ];
  $schema['columns']['layout'] = [
    'type' => 'varchar',
    'length' => 64,
  ];
  $schema['columns']['region'] = [
    'type' => 'varchar',
    'length' => 64,
  ];
}

/**
 * Prepares variables for `field.html.twig`.
 */
function bricks_layouts_preprocess_field(&$variables) {
  $element = $variables['element'];

  if (in_array($element['#formatter'], ['layout_bricks_nested'])) {
    $variables['items'] = [
      [
        'content' => _bricks_layouts_nest_items($element, $variables['items']),
      ],
    ];
  }
}

/**
 * Helper function: converts element's items to a tree structure.
 */
function _bricks_layouts_nest_items($element, $items) {
  $layout = '';
  $elementItems = $element['#items']->getValue();
  foreach ($elementItems as $key => $item) {
    // TODO: refactor.
    if (!empty($item['layout'])) {
      $layout = $item['layout'];
    }
  }
  $layoutPluginManager = \Drupal::service('plugin.manager.core.layout');
  if (!$layoutPluginManager->hasDefinition($layout)) {
    drupal_set_message(t('Layout `%layout_id` is unknown.', ['%layout_id' => $layout]), 'warning');
    return [];
  }
  // Provide any configuration to the layout plugin if necessary.
  $layoutInstance = $layoutPluginManager->createInstance($layout);
  $regionNames = $layoutInstance->getPluginDefinition()->getRegionNames();
  $regions = [];
  foreach ($regionNames as $region) {
    $regions[$region] = [];
  }
  // Sort items by depth.
  foreach ($items as $delta => $item) {
    $region = $elementItems[$delta]['region'];
    // TODO: add depth checking.
    // When JS crashed on node form, we can have the same depth.
    $depth = $elementItems[$delta]['depth'];
    // TODO: For now, cannot render attributes.
    unset($item["attributes"]);
    $regions[$region][$depth] = $item;
  }
  foreach ($regions as $key => $region) {
    ksort($region);
    // TODO: add check for null region.
    $regions[$key] = $region;
  }
  return $layoutInstance->build($regions);
}
